/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.animal;

/**
 *
 * @author nonnanee
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        System.out.println("a1 is aquatic animal ? " + (a1 instanceof AquaticAnimal));
        System.out.println("a1 is poultry animal ? " + (a1 instanceof Poultry));

        Cat c1 = new Cat("Moo");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));

        Animal a2 = c1;
        System.out.println("a2 is land animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animal ? " + (a2 instanceof Reptile));
        System.out.println("a2 is aquatic animal ? " + (a2 instanceof AquaticAnimal));
        System.out.println("a2 is poultry animal ? " + (a2 instanceof Poultry));

        Dog d1 = new Dog("Gram");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));

        Animal a3 = d1;
        System.out.println("a3 is land animal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ? " + (a3 instanceof Reptile));
        System.out.println("a3 is aquatic animal ? " + (a3 instanceof AquaticAnimal));
        System.out.println("a3 is poultry animal ? " + (a3 instanceof Poultry));
        
        Crocodile co = new Crocodile("Jim");
        co.crawl();
        co.eat();
        co.walk();
        co.speak();
        co.sleep();
        System.out.println("co is animal ? " + (co instanceof Animal));
        System.out.println("co is reptile animal ? " + (co instanceof Reptile));

        Animal a4 = co;
        System.out.println("a4 is land animal ? " + (a4 instanceof LandAnimal));
        System.out.println("a4 is reptile animal ? " + (a4 instanceof Reptile));
        System.out.println("a4 is aquatic animal ? " + (a4 instanceof AquaticAnimal));
        System.out.println("a4 is poultry animal ? " + (a4 instanceof Poultry));
        
        Snake s1 = new Snake("Jin");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile animal ? " + (s1 instanceof Reptile));

        Animal a5 = s1;
        System.out.println("a5 is land animal ? " + (a5 instanceof LandAnimal));
        System.out.println("a5 is reptile animal ? " + (a5 instanceof Reptile));
        System.out.println("a5 is aquatic animal ? " + (a5 instanceof AquaticAnimal));
        System.out.println("a5 is poultry animal ? " + (a5 instanceof Poultry));
        
        Crab cr = new Crab("Gim");
        cr.swim();
        cr.eat();
        cr.walk();
        cr.speak();
        cr.sleep();
        System.out.println("cr is animal ? " + (cr instanceof Animal));
        System.out.println("cr is aquatic animal ? " + (cr instanceof AquaticAnimal));

        Animal a6 = cr;
        System.out.println("a6 is land animal ? " + (a6 instanceof LandAnimal));
        System.out.println("a6 is reptile animal ? " + (a6 instanceof Reptile));
        System.out.println("a6 is aquatic animal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is poultry animal ? " + (a6 instanceof Poultry));
        
        Fish f1 = new Fish("Mail");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ? " + (f1 instanceof AquaticAnimal));

        Animal a7 = f1;
        System.out.println("a7 is land animal ? " + (a7 instanceof LandAnimal));
        System.out.println("a7 is reptile animal ? " + (a7 instanceof Reptile));
        System.out.println("a7 is aquatic animal ? " + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is poultry animal ? " + (a7 instanceof Poultry));
        
        Bat b1 = new Bat("Kill");
        b1.fly();
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is poultry animal ? " + (b1 instanceof Poultry));

        Animal a8 = b1;
        System.out.println("a8 is land animal ? " + (a8 instanceof LandAnimal));
        System.out.println("a8 is reptile animal ? " + (a8 instanceof Reptile));
        System.out.println("a8 is aquatic animal ? " + (a8 instanceof AquaticAnimal));
        System.out.println("a8 is poultry animal ? " + (a8 instanceof Poultry));
        
        Bird b2 = new Bird("Kim");
        b2.fly();
        b2.eat();
        b2.walk();
        b2.speak();
        b2.sleep();
        System.out.println("b2 is animal ? " + (b2 instanceof Animal));
        System.out.println("b2 is poultry animal ? " + (b2 instanceof Poultry));

        Animal a9 = b2;
        System.out.println("a9 is land animal ? " + (a9 instanceof LandAnimal));
        System.out.println("a9 is reptile animal ? " + (a9 instanceof Reptile));
        System.out.println("a9 is aquatic animal ? " + (a9 instanceof AquaticAnimal));
        System.out.println("a9 is poultry animal ? " + (a9 instanceof Poultry));
    }
}
